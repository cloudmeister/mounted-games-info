package main

import (
  "log"
  "net/http"
  "os"
  "fmt"
)

func main() {
  fs := http.FileServer(http.Dir("./static"))
  http.Handle("/", fs)

  port := os.Getenv("PORT")
  if port == "" {
    port = "3000"
  }
  log.Println(fmt.Sprintf("Listening on %s", port))
  err := http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
  if err != nil {
    log.Fatal(err)
  }
}
